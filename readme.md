## Article templates with CC BY attribution

Following the recommendations found [here](https://www.ouvrirlascience.fr/mettre-en-oeuvre-la-strategie-de-non-cession-des-droits-sur-les-publications-scientifiques), these templates provide a simple way to emphasize the CC-BY 4.0 license in your article submission.
**They are not to be used on the final version to be published.** You can use these templates for your initial submission and for the archival of the pre-print and accepted versions.

The files have been tested on Overleaf environments using the pdfLaTeX compiler.
